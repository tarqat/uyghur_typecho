<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<?php $this->need('header.php'); ?>

<article class="content-post post tag-jian-ti-zhong-wen" role="main">

  <div class="content-post-title">
    <h1><?php $this->title() ?></h1>
  </div>
  <div class="content-post-meta">
    <time class="post-date" datetime="<?php $this->date('Y-m-d'); ?>"><?php $this->date('Y-m-d'); ?></time> · <?php $this->category(', '); ?></div>
  <div class="content-post-body">
  <?php $this->content(); ?>
  </div>
  <div class="content-post-meta post-meta-tags">
    <?php $this->tags(' , ', true, 'none'); ?>
  </div>
  <div class="content-post-author">
    <div class="tile">
      <div class="tile-icon">
        <figure class="gavatar avatar-lg">
          <img src="<?php $this->options->themeUrl('affinity.png'); ?>" />
        </figure>
      </div>
      <div class="tile-content">
        <p class="tile-title"><strong>تارقات</strong></p>
            <p class="tile-subtitle">ھەمكارلىق ۋە ئۈگۈنۈش مۇساپىمىز مۇشۇ يەردىن باشلانغۇسى......</p>
      </div>
    </div>
  </div>

  <div class="content-post-comments">
  </div>

  <div class="doc_comments">
  <?php $this->need('comments.php'); ?>
  </div>

</article>


<?php $this->need('footer.php'); ?>
