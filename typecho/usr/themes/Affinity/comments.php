<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<script>function tg_c(id,nc){var e=document.getElementById(id);var c=e.className;if(!c){e.className=nc}else{var classArr=c.split(' ');var exist=false;for(var i=0;i<classArr.length;i++){if(classArr[i]===nc){classArr.splice(i,1);e.className=Array.prototype.join.call(classArr," ");exist=true;break}}if(!exist){classArr.push(nc);e.className=Array.prototype.join.call(classArr," ")}}}</script>
<?php function threadedComments($comments, $options) {
    $cl = $comments->levels > 0 ? 'c_c' : 'c_p';
    $author = $comments->url ? '<a href="' . $comments->url . '"'.'" target="_blank"' . ' rel="external">' . $comments->author . '</a>' : $comments->author;
?>
<li id="li-<?php $comments->theId();?>" class="<?php echo $cl;?>">
<div id="<?php $comments->theId(); ?>">
<?php $a = 'https://gravatar.css.network/avatar/' . md5(strtolower($comments->mail)) . '?s=80&r=X&d=mm';?>
    <img class="avatar" src="<?php echo $a ?>" alt="<?php echo $comments->author; ?>" />
    <div class="cp">
    <?php $comments->content(); ?>
    <div class="cm"><span class="ca"><?php echo $author ?></span> <?php $comments->date(); ?><span class="cr"><?php $comments->reply(); ?></span></div>
    </div>
</div>
<?php if ($comments->children){ ?><div class="children"><?php $comments->threadedComments($options); ?></div><?php } ?>
</li>
<?php } ?>
<div id="comments" class="cf">
<?php $this->comments()->to($comments); ?>
<?php if ($comments->have()): ?>
    <h4><?php $this->commentsNum(_t('ھازىرچە باھا يوق'), _t('1 پارچە باھا'), _t('جەمى %d باھا')); ?></h4>
    <?php $comments->listComments(); ?><?php $comments->pageNav('&laquo;', '&raquo;'); ?>
<?php endif; ?>
<div id="<?php $this->respondId(); ?>" class="respond">
    <div class="ccr"><?php $comments->cancelReply(); ?></div>
    <h5 class="response">يىڭى باھا يوللاش</h5>
<form method="post" action="<?php $this->commentUrl() ?>" id="cf" role="form">
<?php if($this->user->hasLogin()): ?>
    <span>تىزىملاتقان<a href="<?php $this->options->profileUrl(); ?>"><?php $this->user->screenName(); ?></a>. <a href="<?php $this->options->logoutUrl(); ?>" title="Logout">چىكىنىش &raquo;</a></span>
<?php else: ?>
    <?php if($this->remember('author',true) != "" && $this->remember('mail',true) != "") : ?>
        <span><?php $this->remember('author'); ?> &nbsp;قايتىپ كەلگىنىڭىزنى قارشى ئالىمىز &nbsp;| <small style="cursor: pointer;" onclick = "tg_c('ainfo','hinfo');">ئارخىپ ئۆزگەرتىش</small></span>
        <div id ="ainfo" class="ainfo hinfo">
    <?php else : ?>
        <div class="ainfo">
        <?php endif ; ?>
        <div class="tbox">
        <input type="text" name="author" id="author" class="ci" placeholder="نام" value="<?php $this->remember('author'); ?>" required>
        <input type="email" name="mail" id="mail" class="ci" placeholder="ئلخەت" value="<?php $this->remember('mail'); ?>" <?php if ($this->options->commentsRequireMail): ?> required<?php endif; ?>>
        <input type="url" name="url" id="url" class="ci" placeholder="تور بىكەت" value="<?php $this->remember('url'); ?>" <?php if ($this->options->commentsRequireURL): ?> required<?php endif; ?>>
        </div>
        </div>
    <?php endif; ?>
    <div class="tbox"><textarea name="text" id="textarea" class="ci" onkeydown="if(event.ctrlKey&&event.keyCode==13){document.getElementById('submit').click();return false};" placeholder="باھايىڭىزنى بۇيەرگە تولدۇرۇڭ" required ><?php $this->remember('text',false); ?></textarea></div>
    <button type="submit" class="submit btn btn-primary" id="submit">تاپشۇرۇش (Ctrl + Enter)</button>
</form>
</div>
</div>