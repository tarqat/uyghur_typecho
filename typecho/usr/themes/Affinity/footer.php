<?php if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>

</section class="section-content" id="content" role="main">
 

      <section class="section-footer">
        <p><strong><a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a></strong> &copy; <?php echo date('Y'); ?> All Rights Reserved.</p>
		<p>Theme Design by <a href="https://github.com/Showfom/Affinity" target="_blank" title="Ghost Theme Affinity">Affinity</a> <span class="text-red">&hearts;</span></p>
          <p>Translate by <strong><a href="http://www.tarqat.com/" target="_blank" title="TARQAT">TARQAT</a></strong>.</p>
      </section>
    </section>
<?php $this->footer(); ?>
</body>
</html>
